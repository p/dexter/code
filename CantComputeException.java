class CantComputeException extends ArithmeticException {
	static final long serialVersionUID=20060308L;

	CantComputeException(String message)
	{
		super(message);
	}
}
