class MissingData extends Exception 
{
	static final long serialVersionUID=20060308L;

	MissingData(String message)
	{
		super(message);
	}

}
